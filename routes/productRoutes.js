/* ===================== REQUIRED MODULES ===================== */

const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productControllers");

/* ============================================================ */


/* =========== Route to retrieve all active products ========== */

router.get("/", (req, res) => {

    productController.getAllActive(req.body).then(resultFromController => res.send(resultFromController));
}) 

/* ============================================================ */


/* ============= Route to retrieve a single product =========== */

router.get("/:productId", (req, res) => {

    productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
}) 

/* ============================================================ */


/* ================= Route to create a product ================ */

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {

        res.send("User is not allowed to create a product.")

    } else {

        productController.addProduct(req.body).then(
            resultFromController => res.send(resultFromController));
    }
})

/* ============================================================ */


/* ============ Route to update product information =========== */

router.put("/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {

        res.send("User is not allowed to update a product.")

    } else {

        productController.updateProduct(req.params.productId, req.body).then(
            resultFromController => res.send(resultFromController));
    }

}) 

/* ============================================================ */


/* ================= Route to archive a product ================ */

router.put("/:productId/archive", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {

        res.send("User is not allowed to archive a product.")

    } else {

        productController.archiveProduct(req.params.productId).then(
            resultFromController => res.send(resultFromController));
    }

}) 

/* ============================================================ */


/* ==================== ARCHIVE ALL PRODUCTS ================== */

router.post("/archiveAll", auth.verify, (req, res) => {

    productController.archiveAllProducts(req.body).then(resultFromController => res.send(resultFromController));
}) 

/* ============================================================ */


/* ==================== DELETE ALL PRODUCTS ================== */

router.delete("/deleteAll", auth.verify, (req, res) => {

    productController.deleteAllProducts(req.body).then(resultFromController => res.send(resultFromController));
}) 

/* ============================================================ */


module.exports = router;
