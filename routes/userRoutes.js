/* ===================== REQUIRED MODULES ===================== */

const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userControllers");
const orderController = require("../controllers/orderControllers");

/* ============================================================ */


/* ============ Route for checking if email exists ============ */

router.post("/checkEmail", (req, res) => {

    userController.checkEmailExists(req.body).then(resultFromController => {
        res.send(resultFromController)
    });

})

/* ============================================================ */


/* ================ Route for User Registration =============== */

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

/* ============================================================ */


/* =============== Route for User Authentication ============== */

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

/* ============================================================ */


/* ============= Route for User Details Retrieval ============= */

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

/* ============================================================ */


/* ============= Route for Setting a User as Admin ============ */

router.post("/:userId/setAsAdmin", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        userController.setAsAdmin(req.params.userId).then(resultFromController => res.send(resultFromController))
        
    } else {

        res.send("You cannot change the user to Admin. Only Admins have this functionality.")
    }

})

/* ============================================================ */


/* ================== Route for Creating Order ================ */

router.post("/checkout", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        res.send("You are an Admin. Only Non-Admins can create orders.")
        
    } else {

        orderController.createOrder(userData.id, req.body).then(resultFromController => res.send(resultFromController))
        
    }

})

/* ============================================================ */


/* ============== Route for Retrieving ALL orders ============= */

router.get("/orders", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
        
    } else {

        res.send("You cannot retrieve all orders. Only Admins have this functionality.")
    }

})

/* ============================================================ */


/* ===== Route for Retrieving Authenticated User's orders ===== */

router.get("/myOrders", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        res.send("You are an Admin. Only Non-Admins can retrieve their specific orders.")
        
    } else {

        orderController.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController))
    }

})

/* ============================================================ */


module.exports = router;