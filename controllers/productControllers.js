/* ===================== REQUIRED MODULES ===================== */

const mongoose = require("mongoose");
const Product = require("../models/Product");

/* ============================================================ */

/* ======= Controller for retrieving all active products ====== */

module.exports.getAllActive = (reqBody) => {
  return Product.find({ isActive: true }).then((result) => {
    if (result.length > 0) {
      return result;
    } else {
      return { result: false };
    }
  });
};

/* ============================================================ */

/* ======== Controller for retrieving a single product ======== */

module.exports.getProduct = async (productId) => {
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return { result: false };
  } else {
    return Product.findById(productId).then((result) => {
      if (result == null) {
        return null;
      } else {
        return result;
      }
    });
  }
};

/* ============================================================ */

/* ============= Controller for creating a product ============ */

module.exports.addProduct = async (reqBody) => {
  let newProduct = new Product({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
    category: reqBody.category,
    stocks: reqBody.stocks,
  });

  return newProduct.save().then((product, error) => {
    if (error) {
      return "Error encountered while saving product in the database.";
    } else {
      return product;
    }
  });
};

/* ============================================================ */

/* ======== Controller for updating product information ======= */

module.exports.updateProduct = async (productId, reqBody) => {
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return "Please provide correct Product Id.";
  } else {
    return Product.findById(productId).then((result) => {
      if (result == null) {
        return "Product not found.";
      } else {
        (result.name = reqBody.name),
          (result.description = reqBody.description),
          (result.price = reqBody.price),
          (result.category = reqBody.category),
          (result.stocks = reqBody.stocks),
          (result.isActive = reqBody.isActive);

        return result.save().then((product, error) => {
          if (error) {
            return "Error encountered while updating product information.";
          } else {
            return product;
          }
        });
      }
    });
  }
};

/* ============================================================ */

/* ============ Controller for archiving a product ============ */

module.exports.archiveProduct = async (productId) => {
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return "Please provide correct Product Id.";
  } else {
    return Product.findById(productId).then((result) => {
      if (result == null) {
        return "Product not found.";
      } else if (result.isActive) {
        result.isActive = false;

        return result.save().then((product, error) => {
          if (error) {
            return "Error encountered while archiving product.";
          } else {
            return product;
          }
        });
      } else {
        return "Product is already archived.";
      }
    });
  }
};

/* ============================================================ */

/* ==================== ARCHIVE ALL PRODUCTS ================== */

module.exports.archiveAllProducts = async (reqBody) => {
  return Product.updateMany({}, { isActive: false }).then((result, error) => {
    if (error) {
      return "Error encountered while archiving all products.";
    } else {
      return "ARCHIVED ALL PRODUCTS.";
    }
  });
};

/* ============================================================ */

/* ==================== DELETE ALL PRODUCTS ================== */

module.exports.deleteAllProducts = async (reqBody) => {
  return Product.deleteMany({}).then((result, error) => {
    if (error) {
      return "Encountered error while deleting all products.";
    } else {
      return "DELETED EVERYTHING! LAGOT KA!";
    }
  });
};

/* ============================================================ */
