/* ===================== REQUIRED MODULES ===================== */

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");

/* ============================================================ */

/* ============ Controller to check if email exists =========== */

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

/* ============================================================ */

/* ============= Controller for User Registration ============= */

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    fullName: reqBody.fullName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return error;
    } else {
      return user;
    }
  });
};

/* ============================================================ */

/* ============ Controller for User Authentication ============ */

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return { access: false };
      }
    }
  });
};

/* ============================================================ */


/* ========== Controller for User Details Retrieval =========== */

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

/* ============================================================ */

/* ========== Controller for Setting a User as Admin ========== */

module.exports.setAsAdmin = async (userId) => {
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return "Please provide correct User Id.";
  } else {
    return User.findById(userId).then((result) => {
      if (result == null) {
        return "User is not yet registered.";
      } else if (result.isAdmin == true) {
        return "User is already an Admin.";
      } else {
        result.isAdmin = true;

        return result.save().then((user, error) => {
          if (error) {
            return "Error encountered while saving.";
          } else {
            return "User is now changed to Admin.";
          }
        });
      }
    });
  }
};

/* ============================================================ */
