/* ===================== REQUIRED MODULES ===================== */

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); 
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

/* ============================================================ */


/* ====================== PORT ASSIGNMENT ===================== */

const port = 8000;

/* ============================================================ */


/* ==================== DATABASE CONNECTION =================== */

const mongoConnectionURL = "mongodb+srv://admin:admin123@course-booking.2xxzg.mongodb.net/test?retryWrites=true";

const databaseName = "Capstone2";

const options = {
    useUnifiedTopology: true,
    useUnifiedTopology: true,
    dbName: databaseName,
};

mongoose.connect(mongoConnectionURL, options).then(() => console.log("Connected to MongoDB.")).catch((error) => console.log(`Error connecting to MongoDB ${error}`));

/* ============================================================ */


/* ========================== RESOURCES ======================= */

const app = express();

app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

/* ============================================================ */


/* =========================== OUTPUT ========================= */

app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`)
});

/* ============================================================ */