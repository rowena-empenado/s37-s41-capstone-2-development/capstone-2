const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Product Name is required."]
    },
    description: {
        type: String,
        required: [true, "Description is required."]
    },
    price: {
        type: Number,
        required: [true, "Price is required."]
    },
    category: {
        type: String,
        required: [true, "Product Category is required."]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    stocks: {
        type: Number,
        required: [true, "Number of stocks is required."]
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orderedIn: [
        {
            orderId: {
                type: String,
                required: [true, "Order Id is required."]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Product", productSchema);